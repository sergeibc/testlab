package com.bcaster.tsto.init

import android.content.Context
import com.facebook.drawee.backends.pipeline.Fresco

object InitHelper {
    fun initComponents(applicationContext: Context?) { //do nothing
        Fresco.initialize(applicationContext)
    }
}