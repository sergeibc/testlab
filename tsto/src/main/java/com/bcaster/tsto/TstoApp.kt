package com.bcaster.tsto

import android.content.Context
import com.bcaster.tsto.init.InitHelper


/**
 *   Created by sergei on 2019-12-09
 */

/**
 * TODO: Add a class header comment!
 */
 
object TstoApp {
    private var applicationContext: Context? = null

    @Synchronized
    fun init(appContext: Context){
        this.applicationContext = appContext.applicationContext
        InitHelper.initComponents(appContext.applicationContext)
    }
}