package com.domain.testlibapp

import android.app.Application
import com.bcaster.tsto.TstoApp


/**
 *   Created by sergei on 2019-12-09
 */

/**
 * TODO: Add a class header comment!
 */
 
class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        TstoApp.init(applicationContext)
    }
}